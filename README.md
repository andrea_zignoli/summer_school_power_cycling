# INTERNATIONAL SUMMER SCHOOL ON WEARABLE SENSORS IN SPORT

In the last few years, the rapid spread of new wearable technologies in the sport and health industries is enabling the collection of an overwhelming amount of data. As we further progress in our data collection we soon realize that we need to make the best out of our sensors and make an efficient usage of the available data. In this module, we will explore different methodologies that aim at improving your ability in 1) correctly interpret the data, 2) discern what is useful from what is useless and 3) make data-driven and objective choices. By using real-world training data files, we will develop interpretative modes (that you can easily deploy on spreadsheets, Matlab or Python scripts) with a hands-on approach. Hopefully, you will soon realize that, with the right tools in your hands, even a single training data file can hide a gold mine of information.

## Description

The goal of this specific repository is to create a place where to create and share the code that can be used to play around with Garmin data files. At first, data coming from 2 professional cyclists competing at the Giro d'Italia are provided. A Matlab script is used to convert tcx files to more standardized txt files, that you can open with a spreadsheet.

## Dependencies

The following software have been used: 

* Matlab 2020a 
* Python 3.9 
* Excel 16.49

## Getting Started

Directly download the repo, or:

```bash
git clone https://andrea_zignoli@bitbucket.org/andrea_zignoli/summer_school_power_cycling.git
```

## Executing program

### First task 

* Download the zip file provided by the organizers
* Unpack and place the data files in the data directory
* Read and convert tcx files: run the read_tcx.m, select a single txt file at time and check the new generated files
* Play around with data

## Help

Please email at andrea.zignoli@unitn.it for further requests/information. 

## Authors

Contributors names and contact info

* [Andrea Zignoli](https://www.researchgate.net/profile/Andrea-Zignoli) 

## Version History

* 0.1
    * Initial Release

## License

This project is licensed under the MIT License - see the LICENSE file for details
