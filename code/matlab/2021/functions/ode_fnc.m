function dx = ode_fnc(t,x,u,P)
%System of equation for VO2 (x1) and AnS (x2)
%   Two first-order equations are provided to describe the dyanmics of
%   these two physiological variables

G       = P(1);
VO2r    = P(2);
tau     = P(3);

% VO2 state var
dx(1) = (u * P(1) + P(2) - x(1)) / P(3);
dx(2) = P(4) - u;

dx = dx';

end

