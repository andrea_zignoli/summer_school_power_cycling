%% get solution data

zeta        = ocp.solution('zeta');
time        = ocp.solution('time');
xLane       = ocp.solution('xLane');
yLane       = ocp.solution('yLane');
xLeftEdge   = ocp.solution('xLeftEdge');
yLeftEdge   = ocp.solution('yLeftEdge');
xRightEdge  = ocp.solution('xRightEdge');
yRightEdge  = ocp.solution('yRightEdge');
AnSources   = ocp.solution('AnSources');
steer       = ocp.solution('steer');
ax          = ocp.solution('ax');
ay          = ocp.solution('ay');
phi         = ocp.solution('phi');
kappa_rider = ocp.solution('alpha_D');
kappa_track = ocp.solution('path_curvature');

race_time = seconds(time(end));
race_time.Format = 'hh:mm:ss'

% robustness for zeta
for j = 1:(length(zeta)-1)
    if zeta(j+1) == zeta(j)
        zeta(j+1) = zeta(j) + 0.1;
    end
end

% power data
OCP_Watts = ocp.solution('Power');
OCP_break = ocp.solution('Power');
% I want to plot only positive powers
OCP_Watts(OCP_Watts<0)=0;
OCP_break(OCP_break>0)=0;
OCP_break = abs(OCP_break);

%% convert in GPS data
% to compute decimal degrees you do
% DD = deg + (min/60) + (sec/3600)
% VERONA TT
lat0    = 45 + 25/60 + 7.66/3600;
long0   = 10 + 58/60 + 52.42/3600;
alt0    = 63;
x0      = 0;
y0      = 0;

[lat_inv,lon_inv,alt_inv] = enu2geodetic(   ocp.solution('xCoM'),...
                                            ocp.solution('yCoM'),...
                                            ones(length(zeta),1),...
                                            lat0,long0,alt0,wgs84Ellipsoid);  
                                        
%% load GPS data
C = importdata('Verona_ITT_HD_full_altitude.txt');
[X,Y,Z] = geodetic2enu(C.data(:,1),C.data(:,2),C.data(:,3),lat0,long0,alt0,wgs84Ellipsoid);
dist(1) = 0;
for i = 2:length(X)
   dist(i) = sqrt( (X(i) - X(i-1))^2 + (Y(i) - Y(i-1))^2 + (Z(i) - Z(i-1))^2 ); 
end
s = cumsum(dist);

%% Compare with experimental data of power 
% best bike split
load best_time_split
[X1,Y1,Z1] = geodetic2enu(results.lat_cut,results.lon_cut,results.alt_cut,lat0,long0,alt0,wgs84Ellipsoid);
for i = 2:length(X1)
    s1(i) = sqrt((X1(i) - X1(i-1))^2 + (Y1(i) - Y1(i-1))^2);
end    
results1 = results;
d1 = cumsum(s1);
clear results

return


%% DATA

figure('Position', [100 0 600 1200])
subplot(9,1,1)
hold on
plot(s, Z, 'k')
set(gca,'xtick',[]);
set(gca,'xcolor',[1 1 1])
ylabel('El m')
xlim([0 max(s)])
grid minor
fill_between(s, Z.*0, Z, [], colOrangeRed);
yyaxis right
hold on
plot(zeta, ocp.solution('slope'))
subplot(9,1,2)
hold on
plot(zeta, ocp.solution('u') * 3.6, 'linewidth', 1, 'color', colDodgerBlue)
% plot(results1.dist_cut, results1.speed_cut * 3.6, 'linewidth', 1, 'color', colOrangeRed)
ylabel('v km/h')
set(gca,'xtick',[]);
set(gca,'xcolor',[1 1 1])
xlim([0 max(s)])
grid minor
yLimits = get(gca,'YLim');
subplot(9,1,3)
hold on
plot(ocp.solution('zeta'), OCP_break, 'linewidth', 1, 'color', colRed)
plot(ocp.solution('zeta'), OCP_Watts, 'linewidth', 1, 'color', colDodgerBlue)
% plot(results1.dist_cut, results1.Watts_cut, 'linewidth', 1, 'color', colOrangeRed)
set(gca,'xtick',[]);
set(gca,'xcolor',[1 1 1])
ylabel('P W')
xlim([0 max(s)])
grid minor

subplot(9,1,4)
hold on
plot(zeta, AnSources, 'color', colDodgerBlue);
set(gca,'xtick',[]);
set(gca,'xcolor',[1 1 1])
ylabel('AnS_n ')
xlim([0 max(s)])
grid minor

subplot(9,1,5)
hold on
plot(zeta, steer * 180/pi, 'color', colDodgerBlue);
set(gca,'xtick',[]);
set(gca,'xcolor',[1 1 1])
ylabel('\delta deg')
xlim([0 max(s)])
grid minor

subplot(9,1,6)
hold on
plot(zeta, ax./9.81, 'linewidth', 1, 'color', colDodgerBlue)
ylabel('a_x g')
set(gca,'xtick',[]);
set(gca,'xcolor',[1 1 1])
xlim([0 max(s)])
grid minor

subplot(9,1,7)
hold on
plot(zeta, ay./9.81, 'linewidth', 1, 'color', colDodgerBlue)
ylabel('a_y g')
set(gca,'xtick',[]);
set(gca,'xcolor',[1 1 1])
xlim([0 max(s)])
grid minor

subplot(9,1,8)
hold on
plot(zeta, phi*180/pi, 'linewidth', 1, 'color', colDodgerBlue)
ylabel('\Phi deg')
set(gca,'xtick',[]);
set(gca,'xcolor',[1 1 1])
ylim([-30 30])
xlim([0 max(s)])
grid minor

subplot(9,1,9)
hold on
plot(zeta, ocp.solution('n'), 'linewidth', 1, 'color', colDodgerBlue)
% plot(results1.dist_cut, results1.Watts_cut, 'linewidth', 1, 'color', colOrangeRed)
ylabel('n m')
xlabel('   Dist')
xlim([0 max(s)])
grid minor
b=axes('Position',[0.130 .05 .8 1e-12]);
set(b,'Units','normalized');
set(b,'Color','none');
set(b,'xlim',[0 max(time)]);
xlabel(b, 'Time (s)')
set(gca,'XMinorTick','on','YMinorTick','off')
pos = get(gca, 'Position');
%pos(1) = 0.055;
%pos(2) = 0.1;
set(gca, 'Position', pos);


%% MAP
figure('position', [10 10 600 800])
hold on
% left edge
plot(xLeftEdge, yLeftEdge, 'linewidth', 2, 'color', colSilver)
plot(xRightEdge, yRightEdge,'linewidth', 2, 'color', colSilver)
plot(xLane, yLane, '--', 'linewidth', 2, 'color', colSilver)
% contour(X,Y,Z);
plot(ocp.solution('xCoM'), ocp.solution('yCoM'), 'linewidth', 2, 'color', colOrangeRed);
surface_line = surface('XData', [ocp.solution('xCoM') ocp.solution('xCoM')],             ... % N.B.  XYZC Data must have at least 2 cols
    'YData', [ocp.solution('yCoM') ocp.solution('yCoM')],             ...
    'ZData', zeros(numel(ocp.solution('yCoM')),2), ...
    'CData', [ocp.solution('u')*3.6 ocp.solution('u')*3.6],             ...
    'FaceColor', 'none',        ...
    'EdgeColor', 'interp',      ...
    'Marker', 'none', ...
    'linewidth', 2);

colormap summer;
colorbar;

% load XYZ_Yates.mat;
% plot(X, Y, 'm');
axis equal

th=0;
for i = 1:length(zeta)
    if zeta(i)>th
        text(xLane(i), yLane(i), ['  ', num2str(th/1000)])
        plot(xLane(i), yLane(i), 'or')
        th=(th+500);
    end
end 

axis off

figure('position', [10 10 600 800])
hold on
% left edge
plot(xLeftEdge, yLeftEdge, 'linewidth', 2, 'color', colSilver)
plot(xRightEdge, yRightEdge,'linewidth', 2, 'color', colSilver)
plot(xLane, yLane, '--', 'linewidth', 2, 'color', colSilver)
% contour(X,Y,Z);
plot(ocp.solution('xCoM'), ocp.solution('yCoM'), 'linewidth', 2, 'color', colOrangeRed);
s = surface('XData', [ocp.solution('xCoM') ocp.solution('xCoM')],             ... % N.B.  XYZC Data must have at least 2 cols
    'YData', [ocp.solution('yCoM') ocp.solution('yCoM')],             ...
    'ZData', zeros(numel(ocp.solution('yCoM')),2), ...
    'CData', [ocp.solution('Power') ocp.solution('Power')],             ...
    'FaceColor', 'none',        ...
    'EdgeColor', 'interp',      ...
    'Marker', 'none', ...
    'linewidth', 2);

colormap jet;
colorbar;

% load XYZ_Yates.mat;
% plot(X, Y, 'm');
axis equal

th=0;
for i = 1:length(zeta)
    if zeta(i)>th
        text(xLane(i), yLane(i), ['  ', num2str(th/1000)])
        plot(xLane(i), yLane(i), 'or')
        th=(th+500);
    end
end 

axis off
                                       
%% Histograms

OCP_Watts(OCP_Watts==0)=[];
OCP_break(OCP_break==0)=[];

figure('position', [100 100 800 300])
hold on
histogram(OCP_Watts, 50, 'facecolor', colDodgerBlue)
histogram(-OCP_break, 50, 'facecolor', colOrangeRed)
set(gca,'ytick',[])
set(gca,'ycolor',[1 1 1])



