%% This script loads and stores tcx data
% Maybe not working super fine with HR data

function results = read_tcx()

addpath('../../data/');
[file,~] = uigetfile('../../data/.txt');

fid = fopen(file);

HR_flag = false;
n = 0;
i = 1;
tline = fgetl(fid);
while ischar(tline)
    tline = fgetl(fid);
    
    if contains(string(tline),'<Time>')
        i = i + 1;
        tmp = extractAfter(extractBefore(string(tline),"</Time>"), '<Time>');
        foo = split(tmp,':');
        timestr = [foo{1}(end-1:end),':',foo{2}, ':', foo{3}(1:2)];
        [Y, M, D, H, MN, S] = datevec(timestr);
        sec(i) = H*3600+MN*60+S;
    end
    
    if contains(string(tline),'<LatitudeDegrees>')
        lat(i) = str2double(extractAfter(extractBefore(string(tline),"</LatitudeDegrees>"), '<LatitudeDegrees>'));
    end
    
    if contains(string(tline),'<LongitudeDegrees>')
        lon(i) = str2double(extractAfter(extractBefore(string(tline),"</LongitudeDegrees>"), '<LongitudeDegrees>'));
    end
    
    if contains(string(tline),'<AltitudeMeters>')
        alt(i) = str2double(extractAfter(extractBefore(string(tline),"</AltitudeMeters>"), '<AltitudeMeters>'));
    end
    
    if contains(string(tline),'<DistanceMeters>')
        dist(i) = str2double(extractAfter(extractBefore(string(tline),"</DistanceMeters>"), '<DistanceMeters>'));
    end
    
    if contains(string(tline),'<ns3:Watts>')
        Watts(i) = str2double(extractAfter(extractBefore(string(tline),"</ns3:Watts>"), '<ns3:Watts>'));
    end
    
    if contains(string(tline),'<Watts>')
        Watts(i) = str2double(extractAfter(extractBefore(string(tline),"</Watts>"), '<Watts>'));
    end
    
    if HR_flag
        HR(i) = str2double(extractAfter(extractBefore(string(tline),"</Value>"), '<Value>'));
        HR_flag = false;
    end
    
    if contains(string(tline),'<HeartRateBpm>')
        HR_flag = true;
    end
    
    if contains(string(tline),'<Cadence>')
        Cadence(i) = str2double(extractAfter(extractBefore(string(tline),"</Cadence>"), '<Cadence>'));
    end
    
    if contains(string(tline),'<ns3:Speed>')
        Speed(i) = str2double(extractAfter(extractBefore(string(tline),"</ns3:Speed>"), '<ns3:Speed>'));
    end
    
    if contains(string(tline),'<Speed>')
        Speed(i) = str2double(extractAfter(extractBefore(string(tline),"</Speed>"), '<Speed>'));
    end
    
    n = n+1;
end

Watts   = sec;
Cadence = sec;
Speed   = sec;

sec(1)      = [];
Watts(1)    = [];
Cadence(1)  = [];
HR(1)       = [];
lat(1)      = [];
lon(1)      = [];
alt(1)      = [];
dist(1)     = [];
Speed(1)    = [];
time        = sec - sec(1);

% we assume that if some power info is missing, it's at the beginning
% and we inlcude zero values
to_add      = length(time) - length(Watts);
to_add_lat  = length(time) - length(lat);

% power robust
Watts   = [zeros(1, to_add) Watts];
lat     = [zeros(1, to_add_lat) lat];
lon     = [zeros(1, to_add_lat) lon];

fclose(fid);

results.time    = time;
results.Speed   = Speed;
results.Watts   = Watts;
results.Cadence = Cadence;
results.HR      = HR;
results.lon     = lon;
results.lat     = lat;
results.alt     = alt;
results.dist    = dist;

if any(lon==0)
   lon(lon==0) = NaN;
end

if any(lat==0)
   lat(lat==0) = NaN;
end

prompt = {'Enter output file name:'};
dlgtitle = 'File Name';
dims = [1];
definput = {'file_name'};
answer = inputdlg(prompt,dlgtitle,dims,definput);

fileID          = fopen(['../../data/', answer{1}, '.txt'], 'w');
header_string   = 'time(s)\tdist(m)\tlon\tlat\talt(m)\tWatts(W)\tSpeed(m/s)\tCadence(bpm)\tHR(bpm)\n';
fprintf(fileID, header_string);
formatSpec      = '%d\t%4.2f\t%4.6f\t%4.6f\t%4.2f\t%3.1f\t%4.2f\t%d\t%d\n';
for i = 1:min([length(time), length(dist), length(lat), length(lon), length(alt), length(Watts), length(Speed), length(Cadence), length(HR)])
    fprintf(fileID, formatSpec, time(i), dist(i), lat(i), lon(i), alt(i), Watts(i), Speed(i), Cadence(i), HR(i));
end    

fclose(fileID);
