%% preamble
clc
clear all
addpath('../../data/')
addpath('functions')
Figure_Settings

%% load the data
Vr1 = importfile('redebus.txt');
Vr1 = rmmissing(Vr1);

%% load GPS data
lat0        = 45 + 25/60 + 7.66/3600;
long0       = 10 + 58/60 + 52.42/3600;
alt0        = 63;
[X1,Y1,Z1]  = geodetic2enu(Vr1.lon,Vr1.lat,Vr1.altm,lat0,long0,alt0,wgs84Ellipsoid);

% compute slope
slope = zeros(size(X1));
for i = 2:length(X1)
    slope(i) = atan((Vr1.altm(i) - Vr1.altm(i-1))/(Vr1.distm(i) - Vr1.distm(i-1)));
end    
slope(isnan(slope)) = 0;
slope_F             = optimal_filter(Vr1.times, slope, 20);

%% simple models for AnS and VO2
VO20     = 500; % mlO2/min
VO2(1)   = VO20; % mlO2/min
VO2ss(1) = VO20; % mlO2/min
AnS0     = 27000; % J
AnS(1)   = AnS0; % J
P        = [11.5, 350, 25, 318];

for i = 2: length(Vr1.times)
    
    [t,y] = ode45(@(t,x) ode_fnc(t,x,Vr1.WattsW(i),P), ...
        [Vr1.times(i-1) Vr1.times(i)], [VO20, AnS0]);
    VO20        = y(end,1);
    AnS0        = y(end,2);
    VO2ss(i)    = Vr1.WattsW(i) * P(1) + P(2);
    VO2(i)      = VO20 + randn(1) * 200; % some noise to make it realistic :)
	AnS(i)      = AnS0;
end

%% Plot variables
figure('Position', [100 0 600 800])
subplot(5,1,1)
hold on
plot(Vr1.distm, Z1, 'k')
set(gca,'xtick',[]);
set(gca,'xcolor',[1 1 1])
ylabel('El m')
xlim([0 max(Vr1.distm)])
grid minor
fill_between(Vr1.distm, Z1.*0, Z1, [], colForestGreen);
yyaxis right
hold on
plot(Vr1.distm, slope_F)
subplot(5,1,2)
hold on
plot(Vr1.distm, Vr1.Speedms * 3.6, 'linewidth', 1, 'color', colDodgerBlue)
ylabel('v km/h')
set(gca,'xtick',[]);
set(gca,'xcolor',[1 1 1])
xlim([0 max(Vr1.distm)])
grid minor
yLimits = get(gca,'YLim');

subplot(5,1,3)
hold on
plot(Vr1.distm, Vr1.WattsW, 'linewidth', 1, 'color', 'K')
set(gca,'xtick',[]);
set(gca,'xcolor',[1 1 1])
ylabel('P W')
xlim([0 max(Vr1.distm)])
grid minor

subplot(5,1,4)
hold on
plot(Vr1.distm, AnS*0.001, 'color', colDarkOrange);
fill_between(Vr1.distm, AnS.*0, AnS*0.001, [], colDarkYellow);
set(gca,'xtick',[]);
set(gca,'xcolor',[1 1 1])
ylabel('AnS kJ')
xlim([0 max(Vr1.distm)])
grid minor

subplot(5,1,5)
hold on
plot(Vr1.distm, VO2, 'linewidth', 1, 'color', colBlue)
plot(Vr1.distm, VO2ss, 'linewidth', 1, 'color', colOrangeRed)
fill_between(Vr1.distm, VO2, VO2ss, [], colOrange);
ylabel('VO2 mlO2/min')
xlabel('   Dist')
xlim([0 max(Vr1.distm)])
grid minor
b=axes('Position',[0.130 .05 .8 1e-12]);
set(b,'Units','normalized');
set(b,'Color','none');
set(b,'xlim',[0 max(Vr1.times)]);
xlabel(b, 'Time (s)')
set(gca,'XMinorTick','on','YMinorTick','off')
pos = get(gca, 'Position');
%pos(1) = 0.055;
%pos(2) = 0.1;
set(gca, 'Position', pos);

%% plot power MAP
figure('position', [10 10 600 800])
hold on
surface_line = surface('XData', [X1 X1], ...
    'YData', [Y1 Y1], ...
    'ZData', zeros(numel(X1),2), ...
    'CData', [Vr1.WattsW Vr1.WattsW], ...
    'FaceColor', 'none', ...
    'EdgeColor', 'interp', ...
    'Marker', 'none', ...
    'linewidth', 2);

colormap jet;
colorbar;
axis equal
axis off

%% Power Histograms
low_power                   = Vr1.WattsW(Vr1.WattsW<200);
mid_power                   = Vr1.WattsW((Vr1.WattsW>=200) & (Vr1.WattsW<400));
high_power                  = Vr1.WattsW((Vr1.WattsW>=400) & (Vr1.WattsW<600));
extreme_power               = Vr1.WattsW(Vr1.WattsW>=600);

figure('position', [100 100 800 300])
hold on
histogram(low_power,        50, 'facecolor', colDodgerBlue)
histogram(mid_power,        50, 'facecolor', colDarkGreen)
histogram(high_power,       50, 'facecolor', colDarkOrange)
histogram(extreme_power,    50, 'facecolor', colOrangeRed)
set(gca,'ytick',[])
set(gca,'ycolor',[1 1 1])
xlim([5 800])
xlabel('Power W')
ylabel('Power distribution (occurrencies)')
title('Racing power distribution (+ZONES)')

