%% ODE CP - Wbal omdel

function dydt = f(t,x,P,u)
% Extract parameters
W_p = P(1);
CP  = P(2);

% Equation to solve
dydt = [-(u-CP)];
end