function [t,P,str,figure_handle,Tw,Tr] = HIT_v2_0(Pw,Pr,Pb,Tw,Tr,Tb,Td,n,m,Tc,flag)
%% HIT training program
% version 2.1
% Total time of the training program is automatically computed with 30s of
% default value before the bouts start
% This function allows you building the HIT program
% Parameters that can be tuned are:
% Pw the power of the exercise [W]
% Pr the power of the recovery phase between bouts [W]
% Pb the basal value of power between sets (0) [W]
% Tw the time of exercise (single bout) [s]
% Tr the time of recovery between bouts [s]
% Tb the time of recovery between sets [s]
% Td default time before and after exercise session [s]
% n number of bouts in a serie [n]
% m number of series in a set [n]
% if flag then plot

disp('HIT training programming version 2.0 Nov 15 AZ')

% Examples
% Pw = 330; % the power of the exercise [W]
% Pr = 80;  % the power of the recovery phase between bouts [W]
% Pb = 0;   % the basal value of power between sets (0) [W]
% Tw = 120; % the time of exercise (single bout) [s]
% Tr = 60;  % the time of recovery between bouts [s]
% Tb = 300; % the time of recovery between series [s]
% Td = 30;  % default time before and after exercise session [s]
% n  = 4;   % number of bouts in a serie [n]
% m  = 2;   % number of series in a set [n]
% Tc = 0.1; % sample time

%initialize power vector
P_multiple = [];

for j = 1:m
    % set
    P_single = [];
    for i = 1:n
        % series
        P_single = [P_single ones(1,Tw)*Pw];
        P_single = [P_single ones(1,Tr)*Pr];
    end
    % build the vector
    P_multiple = [P_multiple P_single ones(1,Tb)*Pb];
    clear P_single
end

Power = [ones(1,Td)*Pb P_multiple ones(1,Td)*Pb];

% if Tc < 1
t = 1:Tc:length(Power);
P = interp1(0:1:(length(Power)-1),Power,t,'linear');
% robustness
P(end) = P(end-1);
% end

% compute the mean of the active part of the training (for one serie)
P_mean = mean(P(t<(t(end) - Td - Tb) & t>Td ));

str = {[num2str(m),'x(', num2str(n),'x(',num2str(Tw),':',num2str(Tr),')s@(',num2str(Pw),':',num2str(Pr),')W'    ]};
disp(str)

%% Plotting, whenever necessary

figure_handle = 0;

if flag
    %
    figure_handle = figure();
    hold on
    grid on
    grid minor
    plot(t, P, 'k', 'linewidth', 4)
    text(Td,20,'\leftarrow Start')
    text(t(end) - Td - Tb, P_mean+0.25*P_mean,' \leftarrow End')
    plot([t(end) - Td - Tb t(end) - Td - Tb], [0 P_mean+0.25*P_mean], 'k', 'linewidth', 1)
    plot([Td t(end) - Td - Tb], [P_mean P_mean], 'r', 'linewidth', 1)
    text(Td,P_mean+0.25*P_mean,[' \downarrow ', num2str(P_mean,3), ' [W]'],'color', [1 0 0], 'fontsize', 14);
    ylabel('Power [W]')
    xlabel('Time [sec]')
    xlim([t(1) round(t(end),2)])
    title(['\fontsize{18} Protocol: ', str]);
    h = legend('Power','Mean power','Location','best');
    set(h,'interpreter','Latex')
    %
end


