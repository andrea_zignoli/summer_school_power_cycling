%% 2022 Summer Scholl in Wearables
% A. Zignoli 14th June 2022

%% Preamble
clc
clear all
close all

Figure_Settings

addpath('functions');
addpath('data');

%% Load the data and compute time series properties

% Read the txt dir
list_files  = dir(fullfile('data','*.txt'));
cell_names  = {list_files.name};

% Set up time window for the moving averages
type        = 'linear';
windowSize  = 30;

time_values = [5, 15, 30, 60, 120, 240, 300, 600, 1200];
maP_values  = [];
MM          = []; 

for i=1:length(cell_names)
    % surf the dir
    tmp         = cell_names(i);
    % create datatable
    tmp_tbl     = readtable(tmp{1});
    % create timetable
    tmp_ttbl    = timetable(seconds(tmp_tbl.time_s_),tmp_tbl.Watts_W_);
    % see https://se.mathworks.com/help/finance/movavg.html#d123e190700 
    % for more details
    maP_values = [];
    for j=1:length(time_values)
        tmp_ma = movavg(tmp_ttbl, type, time_values(j));
        maP_values = [maP_values, max(tmp_ma.Var1)];
    end
    MM = [MM; maP_values];
    
end

%% plot all the MMPs
figure('Position', [100 100 1200, 600]);
subplot(1,2,1)
for i=1:length(MM)
    semilogx(time_values, MM(i,:), '-o')
    hold on
end
semilogx(time_values, max(MM), 'rx')
grid on
xlabel('Log time')
ylabel('Power (W)')
subplot(1,2,2)
for i=1:length(MM)
    plot(time_values, MM(i,:), '-o')
    hold on
end
plot(time_values, max(MM), 'rx')
grid on
xlabel('Time')

%% Plot the max and fit with model

x               = time_values;
y               = max(MM);

CP_model        = @(P,x) P(1)./x + P(2); 
power_law_model = @(P,x) P(1)*x.^(P(2)); 

MSE_CP          = @(P) sum((y - CP_model(P,x)).^2);
MSE_PL          = @(P) sum((y - power_law_model(P,x)).^2);

P0_CP           = [27000; 300];
P_CP            = fminsearch(MSE_CP, P0_CP);  

P0_PL           = [3400; -0.5];
P_PL            = fminsearch(MSE_PL, P0_PL);  

%% Plot fitting
figure('Position', [100 100 1200 600])
semilogx(x, y, 'pg')
hold on
semilogx(x, CP_model(P_CP,x), 'or')
semilogx([1:max(time_values)], CP_model(P_CP, [1:max(time_values)]), 'r')
semilogx(x, power_law_model(P_PL,x), 'ob')
semilogx([1:max(time_values)], power_law_model(P_PL, [1:max(time_values)]), '--b')
xlabel('Log time')
ylabel('Power (W)')
title('Fitting models for experimental data from the field')
grid on
grid minor

%% Implement CP model parameters in Wbal calculation

tmp_tbl = readtable('session_test.txt');
tspan   = 1:length(tmp_tbl.time_s_);
u       = tmp_tbl.Watts_W_;

y0          = [P_CP(1)];
W_bal_tmp   = y0;

for j=1:length(tspan)-1
    [t,q]   = ode45(@(t,x) f(t,x,P_CP,u(j)), [tspan(j), tspan(j+1)], y0);
    Wbal(j) = q(end);
    if Wbal(j) > W_bal_tmp
        Wbal(j) = W_bal_tmp;
    end
    time(j) = t(end);
    y0      = Wbal(j);
end

%% Plot results of the integration
figure('Position', [100 100 1400 600])
subplot(2,1,1)
plot(time, Wbal, 'b')
ylabel('W_{bal} (J)')
grid on
grid minor
subplot(2,1,2)
plot(tspan, u, 'b')
yline(P_CP(2),'--r',{'Critical power'})
ylabel('Power (W)')
xlabel('Time (s)')
grid on
grid minor

%% HIIT session simulation

[tspan,u,str,figure_handle,Tw,Tr] = HIT_v2_0(260,0,0,120,120,240,180,6,2,1,1);

y0          = [P_CP(1)];
W_bal_tmp   = y0;

for j=1:length(tspan)-1
    [t,q]   = ode45(@(t,x) f(t,x,P_CP,u(j)), [tspan(j), tspan(j+1)], y0);
    Wbal(j) = q(end);
    if Wbal(j) > W_bal_tmp
        Wbal(j) = W_bal_tmp;
    end
    time(j) = t(end);
    y0      = Wbal(j);
end
yyaxis right
plot(time, Wbal)
return

%% Load the tcx data

addpath('../../../data/2022/tcx');

% Read the dir
data_dir    = '../../../data/2022/tcx';
list_files  = dir(fullfile(data_dir,'*.tcx'));
cell_names  = {list_files.name};

for i=1:length(cell_names)
    tmp = cell_names(i);
    read_tcx_file(tmp{1});
end










