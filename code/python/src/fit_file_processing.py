import numpy as np
from fitparse import FitFile
import matplotlib.pyplot as plt
import pandas as pd
import datetime
from scipy.optimize import curve_fit

# TODO: write a configuration file
file_name = '../../../data/Verona_TT_1.fit'
fit_file = FitFile(file_name)

# TODO: create class for this
def objective(x, a, b):
	return a * x ** b

# initiate a pd dataframe
Watts = []
lat = []
lon = []
alt = []
dist = []
Speed = []
time_stamps = []
time = []

for record in fit_file.get_messages("record"):
    # Records can contain multiple pieces of data (ex: timestamp, latitude, longitude, etc)
    for data in record:
        # Print the name and value of the data (and the units if it has any)
        if data.name == 'power':
            Watts.append(data.value)
        if data.name == 'position_lat':
            lat.append(data.value/11930465)
        if data.name == 'position_long':
            lon.append(data.value/11930465)
        if data.name == 'altitude':
            alt.append(data.value)
        if data.name == 'distance':
            dist.append(data.value)
        if data.name == 'speed':
            Speed.append(data.value)
        if data.name == 'timestamp':
            time_stamps.append(data.value)

for _time in time_stamps:
    time.append(_time.timestamp() - time_stamps[0].timestamp())
time = np.array(time)

# build the df
end_index = min([len(time), len(dist), len(lon), len(lat), len(alt), len(Watts), len(Speed)])

exp_data = pd.DataFrame()
exp_data['time_stamps'] = time_stamps[0:end_index]
exp_data['time'] = time[0:end_index]
exp_data['dist'] = dist[0:end_index]
exp_data['lon'] = lon[0:end_index]
exp_data['lat'] = lat[0:end_index]
exp_data['alt'] = alt[0:end_index]
exp_data['Watts'] = Watts[0:end_index]
exp_data['Speed'] = Speed[0:end_index]
exp_data = exp_data.drop_duplicates(['time', 'lat', 'lon'])
exp_data = exp_data.drop(exp_data[exp_data.Speed < 3].index)
exp_data = exp_data.reset_index()

# setting first name as index column
exp_data.set_index("time_stamps", inplace=True)
# computing averages
exp_data_avg_30_sec = exp_data.rolling(30, min_periods=1).mean().shift(-29)
exp_data_avg_1_min = exp_data.rolling(60, min_periods=1).mean().shift(-59)
exp_data_avg_5min = exp_data.rolling(300, min_periods=1).mean().shift(-299)
exp_data_avg_10min = exp_data.rolling(600, min_periods=1).mean().shift(-599)

# plot
with plt.style.context('dark_background'):

    title_font = {'fontname': 'AppleGothic'}
    axis_font = {'fontname': 'AppleGothic'}

    fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(12, 8))
    # plt.plot(exp_data_avg.index, exp_data_avg.Watts)
    ax1.fill_between(exp_data.index, exp_data.Watts, 0, color='dodgerblue', alpha=0.25)
    ax1.plot(exp_data_avg_30_sec.index + datetime.timedelta(seconds=30), exp_data_avg_30_sec.Watts, alpha=0.75, linewidth=1,
              color='orangered', label='30 sec')
    ax1.plot(exp_data_avg_1_min.index + datetime.timedelta(seconds=60), exp_data_avg_1_min.Watts, alpha=0.75, linewidth=1,
             color='darkorange', label='1 min')
    ax1.plot(exp_data_avg_5min.index + datetime.timedelta(seconds=300), exp_data_avg_5min.Watts, alpha=0.75, linewidth=1,
             color='darkorchid', label='5 min')
    ax1.plot(exp_data_avg_10min.index + datetime.timedelta(seconds=600), exp_data_avg_10min.Watts, alpha=0.75, linewidth=1,
             color='lime', label='10 min')

    ax2.scatter(30, np.max(exp_data_avg_30_sec.Watts), 40, alpha=0.75, color='orangered', label='30 sec')
    ax2.scatter(60, np.max(exp_data_avg_1_min.Watts), 40, alpha=0.75, color='darkorange', label='1 min')
    ax2.scatter(300, np.max(exp_data_avg_5min.Watts), 40, alpha=0.75, color='darkorchid', label='5 min')
    ax2.scatter(600, np.max(exp_data_avg_10min.Watts), 40, alpha=0.75, color='lime', label='10 min')

    popt, _ = curve_fit(objective, [30, 60, 300, 600], [np.max(exp_data_avg_30_sec.Watts),
                                                        np.max(exp_data_avg_1_min.Watts),
                                                        np.max(exp_data_avg_5min.Watts),
                                                        np.max(exp_data_avg_10min.Watts)])
    ax2.plot(np.arange(5, 600, 1), objective(np.arange(5, 600, 1), popt[0], popt[1]), alpha=0.5,
             linewidth=4,
             color='white', label='Power law')

    ax1.set_ylabel('Power W', **axis_font)
    ax2.set_ylabel('Power W', **axis_font)
    ax2.set_xlabel('Time duration s', **axis_font)
    ax1.grid(c='dimgray', alpha=0.3, linestyle=':')
    ax2.grid(c='dimgray', alpha=0.3, linestyle=':')
    ax1.legend(loc='best')
    ax2.legend(loc='best')
    plt.setp(ax1.get_xticklabels())
    plt.setp(ax1.get_yticklabels(), fontsize=8)
    plt.setp(ax2.get_yticklabels(), fontsize=8)
    title = 'Maximal mean power'
    fig.suptitle(title, y=0.98, **title_font)
    EOF = 1